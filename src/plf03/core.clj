(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (+ (+ x 6) (* x x) x))
        z (comp f g)]
    (z 22)))

(defn función-comp-2
  []
  (let [f (fn [x] (str x))
        g (fn [x] (+ 2 x))
        z (comp f g)]
    (z 80)))

(defn función-comp-3
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (dec x))
        z (comp f g)]
    (z 18)))

(defn función-comp-4
  []
  (let [f (fn [x] (true? x))
        z (comp f)]
    (z 22)))

(defn función-comp-5
  []
  (let [f (fn [x] (take 2 x))
        g (fn [x] (str x))
        z (comp f g)]
    (z 9876543210)))

(defn función-comp-6
  []
  (let [f (fn [x] (char? x))
        g (fn [x] (inc x))
        z (comp f g)]
    (z 10)))

(defn función-comp-7
  []
  (let [f (fn [x] (char? x))
        g (fn [x] (str x))
        z (comp f g)]
    (z 10)))

(defn función-comp-8
  []
  (let [f (fn [x] (+ x x))
        g (fn [x] (* 3 x))
        z (comp f g)]
    (z 10)))

(defn función-comp-9
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (+ 7 x))
        z (comp f g)]
    (z 777)))

(defn función-comp-10
  []
  (let [f (fn [x] (- (* x 3) x))
        g (fn [x] (/ x 3))
        z (comp f g)]
    (z 100)))

(defn función-comp-11
  []
  (let [f (fn [x] (sequential? x))
        g (fn [x] (str x))
        z (comp f g)]
    (z 9876543210)))

(defn función-comp-12
  []
  (let [f (fn [x] (integer? x))
        g (fn [x] (* 20 x))
        z (comp f g)]
    (z 111)))

(defn función-comp-13
  []
  (let [f (fn [x] (sequential? x))
        g (fn [x] (list x))
        z (comp f g)]
    (z 8888)))

(defn función-comp-14
  []
  (let [f (fn [x] (reverse x))
        z (comp f)]
    (z "hello")))

(defn función-comp-15
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (+ 11 x))
        z (comp f g)]
    (z 999)))

(defn función-comp-16
  []
  (let [f (fn [x] (seqable? x))
        g (fn [x] (+ 22 x))
        z (comp f g)]
    (z 555)))

(defn función-comp-17
  []
  (let [f (fn [x] (repeat 3 x))
        z (comp f)]
    (z 200)))

(defn función-comp-18
  []
  (let [f (fn [x] (float? x))
        g (fn [x] (+ 15 x))
        z (comp f g)]
    (z 5.99)))

(defn función-comp-19
  []
  (let [f (fn [x] (concat x "hola"))
        z (comp f)]
    (z "hello")))

(defn función-comp-20
  []
  (let [f (fn [x] (int? x))
        z (comp f)]
    (z 360)))


(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)



(defn función-complement-1
  []
  (let [f (fn [x] (true? x))
        z (complement f)]
    (z true)))

(defn función-complement-2
  []
  (let [f (fn [x] (string? x))
        z (complement f)]
    (z "Hola ¿cómo estás?")))

(defn función-complement-3
  []
  (let [f (fn [x] (true? x))
        z (complement f)]
    (z false)))

(defn función-complement-4
  []
  (let [f (fn [x] (string? x))
        z (complement f)]
    (z 785320329238)))

(defn función-complement-5
  []
  (let [f (fn [x] (empty? x))
        z (complement f)]
    (z [:a :e :i :o :u])))

(defn función-complement-6
  []
  (let [f (fn [x] (map? x))
        z (complement f)]
    (z {:a 100 :b 200 :c 300 :d 400 :e 500})))

(defn función-complement-7
  []
  (let [f (fn [x] (map? x))
        z (complement f)]
    (z 787245)))

(defn función-complement-8
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z "abecedario")))

(defn función-complement-9
  []
  (let [f (fn [x] (empty? x))
        z (complement f)]
    (z #{})))

(defn función-complement-10
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z {:a \a :e \e :i \i :o \o :u \u})))

(defn función-complement-11
  []
  (let [f (fn [x] (even? x))
        z (complement f)]
    (z 99)))

(defn función-complement-12
  []
  (let [f (fn [x] (float? x))
        z (complement f)]
    (z 567.0101)))

(defn función-complement-13
  []
  (let [f (fn [x] (int? x))
        z (complement f)]
    (z 222)))

(defn función-complement-14
  []
  (let [f (fn [x] (seqable? x))
        z (complement f)]
    (z 50)))

(defn función-complement-15
  []
  (let [f (fn [x] (even? x))
        z (complement f)]
    (z 20)))

(defn función-complement-16
  []
  (let [f (fn [x] (sequential? x))
        z (complement f)]
    (z 333)))

(defn función-complement-17
  []
  (let [f (fn [x] (false? x))
        z (complement f)]
    (z true)))

(defn función-complement-18
  []
  (let [f (fn [x] (int? x))
        z (complement f)]
    (z 100.100)))

(defn función-complement-19
  []
  (let [f (fn [x] (sequential? x))
        z (complement f)]
    (z [100 101 102 103 104 105])))

(defn función-complement-20
  []
  (let [f (fn [x] (false? x))
        z (complement f)]
    (z false)))

(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)

(defn función-constantly-1
  []
  (let [f (fn [x] (+ 7/4 x))
        z (constantly f)]
    ((z)10)))

(defn función-constantly-2
  []
  (let [f (fn [x] (even? x))
        z (constantly f)]
    ((z)30)))

(defn función-constantly-3
  []
  (let [f (fn [x] (map? x))
        z (constantly f)]
    ((z){:nombre "Estefania" :apellido "Ortega" :edad "23" :ocupación "Estudiante"})))

(defn función-constantly-4
  []
  (let [f (fn [x] (* 200 x))
        z (constantly f)]
    ((z)156)))

(defn función-constantly-5
  []
  (let [f (fn [x] (seqable? x))
        z (constantly f)]
    ((z)555)))

(defn función-constantly-6
  []
  (let [f (fn [x] (/ x 6))
        z (constantly f)]
    ((z)900)))

(defn función-constantly-7
  []
  (let [f (fn [x] (/ 2 x))
        z (constantly f)]
    ((z)70)))

(defn función-constantly-8
  []
  (let [f (fn [x] (true? x))
        z (constantly f)]
    ((z) false)))

(defn función-constantly-9
  []
  (let [f (fn [x] (map? x))
        z (constantly f)]
    ((z){})))

(defn función-constantly-10
  []
  (let [f (fn [x] (int? x))
        z (constantly f)]
    ((z)101112131415)))

(defn función-constantly-11
  []
  (let [f (fn [x] (false? x))
        z (constantly f)]
    ((z) true)))

(defn función-constantly-12
  []
  (let [f (fn [x] (int? x))
        z (constantly f)]
    ((z)1)))

(defn función-constantly-13
  []
  (let [f (fn [x] (empty? x))
        z (constantly f)]
    ((z) [5 65 981 15])))

(defn función-constantly-14
  []
  (let [f (fn [x] (true? x))
        z (constantly f)]
    ((z) true)))

(defn función-constantly-15
  []
  (let [f (fn [x] (sequential? x))
        z (constantly f)]
    ((z)[100 200 300 400 500])))

(defn función-constantly-16
  []
  (let [f (fn [x] (+ 83 x))
        z (constantly f)]
    ((z)163)))

(defn función-constantly-17
  []
  (let [f (fn [x] (even? x))
        z (constantly f)]
    ((z)21)))

(defn función-constantly-18
  []
  (let [f (fn [x] (int? x))
        z (constantly f)]
    ((z)100.00)))

(defn función-constantly-19
  []
  (let [f (fn [x] (false? x))
        z (constantly f)]
    ((z) false)))

(defn función-constantly-20
  []
  (let [f (fn [x] (sequential? x))
        z (constantly f)]
    ((z) [90 89 88 87 86 85])))


(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-12)
(función-constantly-11)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

(defn función-every-pred-1
  []
  (let [f (fn [x] (number? x))
        g (fn [y] (pos? y))
        z (every-pred f g)]
    (z 10)))

(defn función-every-pred-2
  []
  (let [f (fn [x] (empty? x))
        g (fn [x] (list? x))
        h (fn [x] (vector? x))
        z (every-pred f g h)]
    (z '())))

(defn función-every-pred-3
  []
  (let [f (fn [x] (map? x))
        g (fn [x] (empty? x))
        z (every-pred f g)]
    (z {:nombre "Estefania" :apellido "Ortega" :edad "23" :ocupación "Estudiante"})))

(defn función-every-pred-4
  []
  (let [f (fn [x] (list? x))
        g (fn [x] (every? even? x))
        z (every-pred f g)]
    (z (list 2 4 6))))

(defn función-every-pred-5
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (pos? x))
        h (fn [x] (neg? x))
        z (every-pred f g h)]
    (z 1)))

(defn función-every-pred-6
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (char? x))
        z (every-pred f g)]
    (z "Hello")))

(defn función-every-pred-7
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (double? x))
        z (every-pred f g)]
    (z 1.111 9.30)))

(defn función-every-pred-8
  []
  (let [f (fn [x] (true? x))
        g (fn [x] (boolean? x))
        z (every-pred f g)]
    (z false)))

(defn función-every-pred-9
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (not-every? odd? x))
        z (every-pred f g)]
    (z [1 2 3 4 5])))

(defn función-every-pred-10
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (pos? x))
        h (fn [x] (even? x))
        z (every-pred f g h)]
    (z 222)))

(defn función-every-pred-11
  []
  (let [f (fn [x] (false? x))
        g (fn [x] (true? x))
        h (fn [x] (boolean? x))
        z (every-pred f g h)]
    (z true)))

(defn función-every-pred-12
  []
  (let [f (fn [x] (empty? x))
        g (fn [x] (nil? x))
        z (every-pred f g)]
    (z nil)))

(defn función-every-pred-13
  []
  (let [f (fn [x] (not-empty x))
        g (fn [x] (some char? x))
        z (every-pred f g)]
    (z [\a \e \i \o \u])))

(defn función-every-pred-14
  []
  (let [f (fn [x] (true? x))
        g (fn [x] (boolean? x))
        h (fn [x] (false? x))
        z (every-pred f g h)]
    (z true)))

(defn función-every-pred-15
  []
  (let [f (fn [x] (sequential? x))
        g (fn [x] (vector? x))
        h (fn [x] (every? int? x))
        z (every-pred f g h)]
    (z [100 200 300 400 500])))

(defn función-every-pred-16
  []
  (let [f (fn [x] (symbol? x))
        g (fn [x] (char? x))
        z (every-pred g f)]
    (z \b)))

(defn función-every-pred-17
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (pos? x))
        h (fn [x] (zero? x))
        z (every-pred f g h)]
    (z 200)))

(defn función-every-pred-18
  []
  (let [f (fn [x] (keyword? x))
        g (fn [x] (symbol? x))
        z (every-pred f g)]
    (z :nombre)))

(defn función-every-pred-19
  []
  (let [f (fn [x] (false? x))
        g (fn [x] (string? x))
        z (every-pred f g)]
    (z false)))

(defn función-every-pred-20
  []
  (let [f (fn [x] (sequential? x))
        g (fn [x] (every? pos? x))
        h (fn [x] (vector? x))
        i (fn [x] (empty? x))
        z (every-pred f g h i)]
    (z [90 89 88 87 86 85])))


(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

(defn función-fnil-1
  []
  (let [f (fn [x] (str "Hola " x))
        y (fnil f "mundo")
        z (fnil f "a todos")]
    [(y "Estefania") (z "Berenice")]))

(defn función-fnil-2
  []
  (let [f (fn [x y] (+ x y))
        z (fnil f nil nil)]
    (z 30 30)))

(defn función-fnil-3
  []
  (let [f (fn [x] (str "Hello " x))
        z (fnil f "world")]
    (z nil)))

(defn función-fnil-4
  []
  (let [f (fn [x y] (str "Dos mascotas: " x " y " y))
        z (fnil f "tortuga" "hamster")]
    (z "perro" "gato")))

(defn función-fnil-5
  []
  (let [f (fn [x] (take 5 x))
        z (fnil f (take 5 (range 10 20)))]
    (z nil)))

(defn función-fnil-6
  []
  (let [f (fn [x] (map true? x))
        z (fnil f (map true? [false true false true]))]
    (z ["true" true true nil])))

(defn función-fnil-7
  []
  (let [f (fn [x] (nil? x))
        z (fnil f (nil? nil))]
    (z nil)))

(defn función-fnil-8
  []
  (let [f (fn [a b c] (str "Nombre completo: " a " " b " " c))
        z (fnil f "Estefania" "Ortega" "Martínez")]
    (z nil "Berenice" nil)))

(defn función-fnil-9
  []
  (let [f (fn [x] (take-last 10 x))
        z (fnil f (take-last 10 (range 50 100)))]
    (z '(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20))))

(defn función-fnil-10
  []
  (let [f (fn [x] (take-last 10 x))
        y (fnil f (take-nth 100 (range 100 500)))
        z (fnil f (take-last 10 (range 50 100)))]
    [(y nil) (z nil)]))

(defn función-fnil-11
  []
  (let [f (fn [x] (take-last 3 x))
        z (fnil f (take 2 (range 10)))]
    (z [\a \e \i \o \u])))

(defn función-fnil-12
  []
  (let [f (fn [x] (map boolean? x))
        z (fnil f "x es nil")]
    (z #{false true})))

(defn función-fnil-13
  []
  (let [f (fn [x] (boolean? x))
        z (fnil f "x es nil")]
    (z nil)))

(defn función-fnil-14
  []
  (let [f (fn [x] (reverse x))
        z (fnil f (+ 2 3))]
    (z "abecedario")))

(defn función-fnil-15
  []
  (let [f (fn [x] (empty? x))
        g (fn [x] (first x))
        y (fnil f nil)
        z (fnil g nil)]
    [(y [1 2 3 4 5]) (z [5 4 3 2 1])]))

(defn función-fnil-16
  []
  (let [f (fn [x] (str "Mi color favorito es el " x))
        z (fnil f "rosa")]
    (z "morado")))

(defn función-fnil-17
  []
  (let [f (fn [x] (str "Mi color favorito es el " x))
        z (fnil f "rosa")]
    (z nil)))

(defn función-fnil-18
  []
  (let [f (fn [w x y] (replace [:primero :segundo :tercero :cuarto :cinco :seis] [w x y]))
        z (fnil f 1 1 1)]
    (z nil nil nil)))

(defn función-fnil-19
  []
  (let [f (fn [w x y] (replace [\a \b \c \d \e \f \g \h \i \j \k \l \m \n \ñ] [w x y]))
        z (fnil f 3 7 11)]
    (z nil 14 nil)))

(defn función-fnil-20
  []
  (let [f (fn [a] (inc a))
        g (fn [a b] (* a b))
        h (fn [a b c] (+ a b c))
        x (fnil f 110)
        y (fnil g 1/2 1/8)
        z (fnil h 98 12 897)]
    [(x nil) (y 6 nil) (z 8/5 nil 3/4)]))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)

(defn función-juxt-1
  []
  (let [f (fn [x] (take 2 x))
        g (fn [x] (drop 4 x))
        z (juxt :a f g)]
    (z {:a 1 :b 2 :c 3 :d 4 :e 5 :f 6 :g 7 :h 8 :i 9 :j 10})))

(defn función-juxt-2
  []
  (let [f (fn [x] (str x))
        g (fn [x] (count x))
        z (juxt f g)]
    (z [[:a "abecedario"] [:b "murcielago"] [:c "casa"]])))

(defn función-juxt-3
  []
  (let [f (fn [x] (shuffle x))
        z (juxt f)]
    (z '(100 200 300 400 500))))

(defn función-juxt-4
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (count x))
        z (juxt f g)]
    (z "Estefania Berenice Ortega Martínez")))

(defn función-juxt-5
  []
  (let [f (fn [x] (reverse x))
        g (fn [x] (nil? x))
        z (juxt f g)]
    (z "chocolates")))

(defn función-juxt-6
  []
  (let [f (fn [x y] (every? x y))
        g (fn [x y] (conj x y))
        z (juxt f g)]
    (z #{10 20 30 40 50} [:a :b :c :d :e])))

(defn función-juxt-7
  []
  (let [f (fn [x y] (find x y))
        z (juxt :c :d f)]
    (z  {:a 80 :b 60 :c 56 :d 89} :c)))

(defn función-juxt-8
  []
  (let [f (fn [x] (vector x))
        g (fn [x] (seq? x))
        z (juxt f g)]
    (z "interestelar")))

(defn función-juxt-9
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (dec x))
        z (juxt f g)]
    (z 121)))

(defn función-juxt-10
  []
  (let [f (fn [x] (reverse x))
        z (juxt :a :b f)]
    (z {:a "amarillo" :b "blanco" :c "café" :d "dorado" :m "morado" :n "negro"})))

(defn función-juxt-11
  []
  (let [f (fn [x] (keys x))
        z (juxt :name f)]
    (z {:name "Estefania" :apellido "Ortega" :edad 23})))

(defn función-juxt-12
  []
  (let [f (fn [x] (vector x))
        z (juxt :a :e f)]
    (z {:a 111 :e 222 :i 333 :o 444 :u 555})))

(defn función-juxt-13
  []
  (let [f (fn [x] (map? x))
        z (juxt :b f)]
    (z {:a \a :b \b :c \c :d \d :e \e :f \f :g \g})))

(defn función-juxt-14
  []
  (let [f (fn [x y] (mapv + x y))
        z (juxt f)]
    (z [1 2 3 4 5] [6 7 8 9 10])))

(defn función-juxt-15
  []
  (let [f (fn [x y] (split-at x y))
        z (juxt f)]
    (z 1 [0 56 23 45 9 6 1 22 40])))

(defn función-juxt-16
  []
  (let [f (fn [x] (into [] x))
        z (juxt :a :c f)]
    (z {:a "otoño" :b "primavera" :c "verano" :d "invierno"})))

(defn función-juxt-17
  []
  (let [f (fn [x] (indexed? x))
        z (juxt f)]
    (z ["hi" "hello" "hola" "holi"])))

(defn función-juxt-18
  []
  (let [f (fn [x] (vector x))
        g (fn [x] (first x))
        z (juxt f g)]
    (z {"hello" "hi" "hola" "holi"})))

(defn función-juxt-19
  []
  (let [f (fn [x] (take-last 3 x))
        z (juxt :c f)]
    (z {:a "arbol" :b "barco" :c "coco" :d "dinosaurio" :e "estudiante" :f "foco" :g "galleta"})))

(defn función-juxt-20
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (last x))
        z (juxt f g)]
    (z "anatomía humana")))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)

(defn función-partial-1
  []
  (let [f (fn [u v w x y] (vector u v w x y))
        z (partial f 100)]
    (z 200 300 400 500)))

(defn función-partial-2
  []
  (let [f (fn [u v w x y] (u v w x y))
        z (partial f +)]
    (z 121 212 111 987)))

(defn función-partial-3
  []
  (let [f (fn [w x y] (w x y))
        z (partial f -)]
    (z 100 101)))

(defn función-partial-4
  []
  (let [f (fn [v w x y] (v w x y))
        z (partial f *)]
    (z 10 10 10)))

(defn función-partial-5
  []
  (let [f (fn [w x y] (w x y))
        z (partial f /)]
    (z 80 20)))

(defn función-partial-6
  []
  (let [f (fn [u v w x y] (hash-map :a u :b v :c w :d x :e y))
        z (partial f "arbol")]
    (z "barba" "carro" "dulce" "estrella")))

(defn función-partial-7
  []
  (let [f (fn [x y] (range x y))
        z (partial f 100)]
    (z 150)))

(defn función-partial-8
  []
  (let [f (fn [x y] (drop-while x y))
        z (partial f neg?)]
    (z [-1 -2 -6 -7 1 2 3 4 -5 -6 0 1])))

(defn función-partial-9
  []
  (let [f (fn [x y] (take-nth x y))
        z (partial f 5)]
    (z (range 50))))

(defn función-partial-10
  []
  (let [f (fn [x y] (partition-by x y))
        z (partial f even?)]
    (z [1 1 1 2 2 3 3])))

(defn función-partial-11
  []
  (let [f (fn [x y] (group-by x y))
        z (partial f count)]
    (z ["te" "ver" "dado" "mimir" "si" "foco" "no"])))

(defn función-partial-12
  []
  (let [f (fn [x y] (merge x y))
        z (partial f {:b 9 :d 4})]
    (z {:a 1 :b 2 :c 3})))

(defn función-partial-13
  []
  (let [f (fn [x y] (select-keys x y))
        z (partial f [:i])]
    (z {:a "amarillo" :e "espejo" :i "iguana" :o "operación" :u "uvas"})))

(defn función-partial-14
  []
  (let [f (fn [x y] (partition-all x y))
        z (partial f 4)]
    (z [0 1 2 3 4 5 6 7 8 9])))

(defn función-partial-15
  []
  (let [f (fn [x y] (into x y))
        z (partial f [])]
    (z {:a "amarillo" :e "espejo" :i "iguana" :o "operación" :u "uvas"})))

(defn función-partial-16
  []
  (let [f (fn [x y] (remove x y))
        z (partial f neg?)]
    (z (vector -1 -2 -4 -5 -7 -8 10 11 13 14 16 17 19 20))))

(defn función-partial-17
  []
  (let [f (fn [x y] (not-any? x y))
        z (partial f odd?)]
    (z '(2 4 6 8 10 12 14 16 18 20))))

(defn función-partial-18
  []
  (let [f (fn [x y] (every? x y))
        z (partial f string?)]
    (z #{"oso" "gato" "perro" "tortuga" "conejo" "caballo"})))

(defn función-partial-19
  []
  (let [f (fn [x y] (zipmap x y))
        z (partial f [:a :b :c :d :e])]
    (z ["araña" "burro" "cangrejo" "dinosaurio" "elefante"])))

(defn función-partial-20
  []
  (let [f (fn [v w x y] (merge-with v w x y))
        z (partial f +)]
    (z {:a 1} {:a 2} {:a 3})))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)

(defn función-some-fn-1
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (pos? x))
        h (fn [x] (boolean? x))
        z (some-fn f g h)]
    (z 10)))

(defn función-some-fn-2
  []
  (let [f (fn [x] (empty? x))
        g (fn [x] (list? x))
        h (fn [x] (vector? x))
        z (some-fn f g h)]
    (z '())))

(defn función-some-fn-3
  []
  (let [f (fn [x] (map? x))
        g (fn [x] (empty? x))
        z (some-fn f g)]
    (z {:nombre "Estefania" :apellido "Ortega" :edad "23" :ocupación "Estudiante"})))

(defn función-some-fn-4
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (every? even? x))
        h (fn [x] (every? string? x))
        z (some-fn f g h)]
    (z (list 2 4 6))))

(defn función-some-fn-5
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (pos? x))
        h (fn [x] (neg? x))
        z (some-fn f g h)]
    (z 1)))

(defn función-some-fn-6
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (char? x))
        z (some-fn f g)]
    (z "Hello")))

(defn función-some-fn-7
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (double? x))
        h (fn [x] (int? x))
        z (some-fn f g h)]
    (z 1.111)))

(defn función-some-fn-8
  []
  (let [f (fn [x] (true? x))
        g (fn [x] (boolean? x))
        h (fn [x] (pos? x))
        z (some-fn f g h)]
    (z false)))

(defn función-some-fn-9
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (not-every? odd? x))
        z (some-fn f g)]
    (z [1 2 3 4 5])))

(defn función-some-fn-10
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (pos? x))
        h (fn [x] (even? x))
        z (some-fn f g h)]
    (z 222)))

(defn función-some-fn-11
  []
  (let [f (fn [x] (true? x))
        g (fn [x] (boolean? x))
        h (fn [x] (false? x))
        z (some-fn f g h)]
    (z true)))

(defn función-some-fn-12
  []
  (let [f (fn [x] (empty? x))
        g (fn [x] (nil? x))
        z (some-fn f g)]
    (z nil)))

(defn función-some-fn-13
  []
  (let [f (fn [x] (empty? x))
        g (fn [x] (char? x))
        z (some-fn f g)]
    (z [\a \e \i \o \u])))

(defn función-some-fn-14
  []
  (let [f (fn [x] (true? x))
        g (fn [x] (boolean? x))
        h (fn [x] (false? x))
        z (some-fn f g h)]
    (z true)))

(defn función-some-fn-15
  []
  (let [f (fn [x] (sequential? x))
        g (fn [x] (vector? x))
        h (fn [x] (every? int? x))
        z (some-fn f g h)]
    (z [100 200 300 400 500])))

(defn función-some-fn-16
  []
  (let [f (fn [x] (symbol? x))
        g (fn [x] (char? x))
        h (fn [x] (keyword? x))
        z (some-fn f g h )]
    (z \b)))

(defn función-some-fn-17
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (pos? x))
        h (fn [x] (zero? x))
        i (fn [x] (neg? x))
        z (some-fn f g h i)]
    (z 200)))

(defn función-some-fn-18
  []
  (let [f (fn [x] (keyword? x))
        g (fn [x] (symbol? x))
        z (some-fn f g)]
    (z :nombre)))

(defn función-some-fn-19
  []
  (let [f (fn [x] (false? x))
        g (fn [x] (string? x))
        z (some-fn f g)]
    (z false)))

(defn función-some-fn-20
  []
  (let [f (fn [x] (sequential? x))
        g (fn [x] (every? pos? x))
        h (fn [x] (vector? x))
        i (fn [x] (empty? x))
        j (fn [x] (list? x))
        z (some-fn f g h i j)]
    (z [90 89 88 87 86 85])))


(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-13)
(función-some-fn-12)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)